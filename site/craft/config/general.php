<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 0,

        // Enable CSRF Protection (recommended, will be enabled by default in Craft 3)
        'enableCsrfProtection' => true,

        // Whether "index.php" should be visible in URLs
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('CRAFTENV_SECURITY_KEY'),

        'aliases' => [
            '@assetUrl' => getenv('CRAFTENV_ASSET_URL'),
            '@assetPath' => getenv('CRAFTENV_ASSET_PATH'),
            '@siteUrl' => getenv('CRAFTENV_SITE_URL'),
        ],

        // site specific configs
        'cloudAssetsPath' => 'https://s3-us-west-2.amazonaws.com/craft-o-matic.newlevant.com/ragtag/assets/',
    ],

    // Dev environment settings
    'local' => [
        // Dev Mode (see https://craftcms.com/support/dev-mode)
        'devMode' => true,
        'cloudAssetsPath' => '/assets/',
    ],

    // Staging environment settings
    'staging' => [
    ],

    // Production environment settings
    'production' => [
    ],
];
