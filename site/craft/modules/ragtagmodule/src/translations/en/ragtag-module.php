<?php
/**
 * Ragtag module for Craft CMS 3.x
 *
 * Custom website-o-matic functions
 *
 * @link      http://marion.newlevant.com
 * @copyright Copyright (c) 2018 Marion Newlevant
 */

/**
 * Ragtag en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('ragtag-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Marion Newlevant
 * @package   RagtagModule
 * @since     1.0.0
 */
return [
    'Ragtag plugin loaded' => 'Ragtag plugin loaded',
];
