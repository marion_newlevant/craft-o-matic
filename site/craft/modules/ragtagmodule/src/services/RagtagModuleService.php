<?php
/**
 * Ragtag module for Craft CMS 3.x
 *
 * Custom website-o-matic functions
 *
 * @link      http://marion.newlevant.com
 * @copyright Copyright (c) 2018 Marion Newlevant
 */

namespace modules\ragtagmodule\services;

use modules\ragtagmodule\RagtagModule;

use Craft;
use craft\base\Component;

/**
 * RagtagModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Marion Newlevant
 * @package   RagtagModule
 * @since     1.0.0
 */
class RagtagModuleService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     RagtagModule::$instance->ragtagModuleService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
