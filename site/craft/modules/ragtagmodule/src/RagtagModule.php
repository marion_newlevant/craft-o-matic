<?php
/**
 * Ragtag module for Craft CMS 3.x
 *
 * Custom website-o-matic functions
 *
 * @link      http://marion.newlevant.com
 * @copyright Copyright (c) 2018 Marion Newlevant
 */

namespace modules\ragtagmodule;

use modules\ragtagmodule\services\RagtagModuleService as RagtagModuleServiceService;

use Craft;
use craft\i18n\PhpMessageSource;
use craft\events\ElementEvent;
use craft\services\Elements;

use yii\base\Event;
use yii\base\Module;

use GuzzleHttp\Client;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Marion Newlevant
 * @package   RagtagModule
 * @since     1.0.0
 *
 * @property  RagtagModuleServiceService $ragtagModuleService
 */
class RagtagModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this module class so that it can be accessed via
     * RagtagModule::$instance
     *
     * @var RagtagModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/ragtagmodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\ragtagmodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/ragtagmodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * Set our $instance static property to this class so that it can be accessed via
     * RagtagModule::$instance
     *
     * Called after the module class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        if (Craft::$app->getRequest()->getIsCpRequest()) {
            // get the globals for g_ragtagConfig
                Event::on(Elements::class, Elements::EVENT_AFTER_SAVE_ELEMENT, function(ElementEvent $event) {
                    $config = Craft::$app->getGlobals()->getSetByHandle('g_ragtagConfig', $event->element->siteId);
                   if ($config->siteIsLive)
                   {

                        $netlifyHook = $config->netlifyHook;
                        // poke netlify with post to https://api.netlify.com/build_hooks/$netlifyHook
                        // todo: we need to throttle these so we don't hammer the server.
                        $client = new Client();
                        $response = $client->post('https://api.netlify.com/build_hooks/'.$netlifyHook);
                        Craft::info('netlify response: '.$response->getStatusCode(), __METHOD__);
//                        $promise = $client->requestAsync('POST', 'https://api.netlify.com/build_hooks/'.$netlifyHook );
//                        $promise->then(function($response) {
//                            Craft::info('netlify hook response: '.$response->getStatusCode(), __METHOD__);
//                        });
                    }
                });
            };

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'ragtag-module',
                '{name} module loaded',
                ['name' => 'Ragtag']
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
}
