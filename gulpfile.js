// package vars
const pkg = require("./package.json");
// environment vars
const env = require("./.env.json");

// gulp
const gulp = require("gulp");

// load all plugins in 'devDependencies' into the variable $
const $ = require('gulp-load-plugins')({
  pattern: ['*'],
  scope: ['devDependencies']
});

const browserSync = $.browserSync.create();

// clean tasks:
gulp.task('clean:css', function(callback) {
  $.del([
    pkg.paths.gulpOutput+'css/**/*.css'
  ]);
  callback();
});

gulp.task('clean:js', function(callback) {
  $.del([
    pkg.paths.gulpOutput+'js/**/*.js'
  ]);
  callback();
});

gulp.task('clean', ['clean:css', 'clean:js']);

// Uses Sass compiler to process styles, adds vendor prefixes, minifies, then
// outputs file to the appropriate location.
gulp.task('build:css', function() {
  return gulp.src(pkg.paths.gulpInput+'sass/**/*.scss')
    .pipe($.sass({includePaths: ['./node_modules']}))
    .pipe($.postcss([ $.autoprefixer() ]))
    .pipe($.cleanCss())
    .pipe(gulp.dest(pkg.paths.gulpOutput+'css/'))
    .on('error', $.fancyLog);
});

gulp.task('build:js', function() {
  // copy the vendor/js stuff straight in
  gulp.src(pkg.paths.gulpInput+'js/vendor/*.js')
    .pipe(gulp.dest(pkg.paths.gulpOutput+'js/vendor'))
    .on('error', $.fancyLog);

  // copy our stuff straight in for use in dev
  gulp.src(pkg.paths.gulpInput+'js/*.js')
    .pipe(gulp.dest(pkg.paths.gulpOutput+'js'))
    .on('error', $.fancyLog);

  // concatinate and minify our js
  return gulp.src(pkg.paths.gulpInput+'js/*.js')
    .pipe($.concat('site.min.js'))
    .pipe($.uglify())
    .pipe(gulp.dest(pkg.paths.gulpOutput+'js'))
    .on('error', $.fancyLog);
});

// Builds site anew.
gulp.task('build', function(callback) {
  $.runSequence('clean',
    ['build:css', 'build:js'],
    callback);
});

// tasks that build and then reload BrowserSync:
gulp.task('build:css:watch', ['build:css'], function(callback) {
  browserSync.reload();
  callback();
});

gulp.task('build:js:watch', ['build:js'], function(callback) {
  browserSync.reload();
  callback();
});

// browserSync
gulp.task('serve', ['build'], function() {
  browserSync.init({
    proxy: env.localDomain
  });

  // watch for these things:

  gulp.watch(pkg.paths.gulpInput+'sass/**/*.scss', ['build:css:watch']);

  gulp.watch(pkg.paths.gulpInput+'js/**/*.js', ['build:js:watch']);

  gulp.watch('site/craft/templates/**/*.twig').on('change', browserSync.reload);
  gulp.watch('site/craft/templates/**/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['serve']);
