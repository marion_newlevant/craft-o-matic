## Overview

This is the Craft build for a proof of concept of using Craft -> Netlify.

We use Craft's multi-site feature. Each candidate has their own site in Craft, their own user group, and their own asset volume, so that their content is isolated from that of other candidates.

## Local Development set up.

You will need a local development environment with php, mysql, apache (or nginx). If you have one you are used to using, it will probably work. I use Vagrant/Homestead (setup described [here](https://nystudio107.com/blog/local-development-with-vagrant-homestead)). [Valet](https://laravel.com/docs/master/valet) will also work, or MAMP. Minimum php required is 7.0 - should also work with 7.1 and maybe 7.2.

You will need to set up a database. Initialize it with a copy of a craft-o-matic db.

You will need to copy `site/craft/example.env.php` to `site/.env.php` and set the `CRAFT_ENVIRONMENT` to `local` and the database credentials.

You will need to copy `example.env.json` to `.env.json`, and change the local domain to match whatever domain you are using for the local install.

Run `npm install` to set thing up for gulp. In `site/craft`, run `composer install` to set things up for Craft.

## What the pieces are:

- assets-src - src files for css and js (build by gulp)
- site - everything that will need to be deployed
- .env.json - local development environment settings
- gulpfile.js - gulp build file

In `site`

- craft - All the pieces of craft that live outside the web-root
- web - the web-root. The `index.php` file in here is where it all begins.

## Developing locally

Once we have actual candidate sites we will have to be _VERY CAREFUL_ not to break them.

General guidelines:

- make database changes on the staging server, and then propagate down to your local database.

- gulp file to build the css and js and watch for changes

## Creating a new Candidate

Eventually I would like to automate this.

1. Pick a unique candidate handle. (We need a convention for candidate handles) - `candidateHandle` for this example.
1. Set things up on netlify. Add a build environment variable `candidate` with value `candidateHandle`. Add a build hook, and record the long string at the end of it (`5a86fe3048769b3845f15a99` e.g.)
1. In Craft, go to Settings -> Sites, create a new site. Use `candidateHandle` for the handle. Set the Base URL to `@web/candidateHandle/`
1. Go to Settings -> Assets, and create a new volume. It should have `candidateHandle` for the handle. It should be an Amazon S3 volume. Copy the Access Key ID and Secret Access Key from one of the other candidate volumes, and set the subfolder to `candidateHandle`
1. Go to Settings -> Users, and create a new User Group. It should have `candidateHandle` for the handle, for permissions, it should have the `candidateHandle` Site, and all the options for the `candidateHandle` Asset Volume.
1. Go to Settings -> Sections, and for each of the sections, turn on the Candidate site.
1. Go to Users, and setup a new user in the new Candidate group. Use the real e-mail address. For permissions, check `Candidate` and the `candidateHandle` user groups
1. Copy the password reset URL for the new user, and give it to the Candidate so that they can login. (password reset URL is only good for a couple of days)

## Dump of more info:

Craft instead of DatoCMS, Gulp instead of Jekyll - pieces of the puzzle.

Each candidate gets a netlify site (currently there's just one: testCandidate1), and a site on the Craft (https://github.com/craftcms/cms) build. They can use the Craft version of their site as a staging server. When things are saved on the Craft site, it uses a netlify webhook to trigger the gulp build, which pulls down all the html from the Craft site, and serves it up as the static site.

From the candidate's point of view, they have Craft vs DatoCMS to enter their content, which I think is a win.

From Ragtag's point of view, there is the Craft site to set up instead of the DatoCMS site - I think either of those setups can be pretty thoroughly automated. And there are Craft templates to build instead of Jekyll ones, which I also think is a win.

I would like to find a time to talk about how this would all work.

Here are the links and credentials for the current setup (which is at proof-of-concept stage). Go ahead and poke at things and see how it works. You can login to the Craft site as Test-Candidate-1 and change things, and the changes will be reflected on the deployed candidate site.

Craft build - https://bitbucket.org/marion_newlevant/craft-o-matic
running on http://192.241.219.128
(this is on my digitalocean.com, just to have it somewhere available.)
control panel: http://192.241.219.128/admin
admin login: admin / ny34CZZz7grHdfTdYyufuCMHe4
candidate login: Test-Candidate-1 / Test-Candidate-1
candidate website (staging): http://192.241.219.128/testCandidate1

Netlify piece - https://github.com/marionnewlevant/netlify-test
Up on netlify: https://app.netlify.com/
logon with email: marion.newlevant@gmail.com / kyG6Z3zHRQ6EGQqYufX2sVVwbj
site: friendly-leakey-00fb66

deployed candidate site: https://friendly-leakey-00fb66.netlify.com/
